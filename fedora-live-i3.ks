
%include fedora-live-base.ks
%include fedora-live-minimization.ks
%include fedora-i3.ks
# Contains configs for i3, i3blocks, dunst, termite, Xresources, GTK+, QT:
%include fedora-i3-configs.ks

%post

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/i3
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

cat >> /etc/rc.d/init.d/livesys << EOF

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
#sed -i 's/^#show-language-selector=.*/show-language-selector=true/' /etc/lightdm/lightdm-gtk-greeter.conf

# set default session, otherwise login will fail
sed -i 's/^#user-session=.*/user-session=i3/' /etc/lightdm/lightdm.conf

# Show harddisk install on the desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop

# Start auth agent and networkmanager applet

cat >> /etc/i3/config << 'FEH'
# authentication agent
exec --no-startup-id /usr/libexec/xfce-polkit
# network-manager applet startup
exec --no-startup-id nm-applet
FEH

# no updater applet in live environment
rm -f /etc/xdg/autostart/org.mageia.dnfdragora-updater.desktop

# this goes at the end after all other changes. 
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

EOF

%end

