# fedora-i3-kickstarts

Kickstart files for creating Fedora LiveCD with i3 userland.

 - wm - i3
 - statusbar - i3blocks
 - notification daemon - dunst
 - terminal - termite (default), urxvt

mod+d launches i3-dmenu-desktop

mod+shift+d launches vanilla dmenu

Colours are made to fit Arc-Dark theme.

Icons - Clarity-albus

Fonts - google-noto, mononoki (original DejaVu fonts removed)

The right file to feed to livecd-creator is fedora-live-i3.ks .
Remove incusion of fedora-i3-configs.ks from fedora-live-i3.ks to get the vanilla looks and configs.

Refer to [Fedora docks](https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/#proc_creating-and-using-live-cd) for a howto.
