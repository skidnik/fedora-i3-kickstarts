%post
mkdir --parent /etc/xdg/dunst
mkdir /etc/{gtk-2.0,gtk-3.0}

cat > /etc/i3/config.keycodes <<'EOF'
# WARNING
# WARNING: This configuration file is a template for the i3-config-wizard to
# WARNING: generate a config which uses keysyms in your current layout. It does
# WARNING: not get loaded by i3. Please do not change it.
# WARNING

# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod1

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 10

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn't scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindcode $mod+36 exec i3-sensible-terminal

# kill focused window
bindcode $mod+Shift+24 kill

# start dmenu (a program launcher)
set $dmenu_run-themed "dmenu_run -i -fn 'monospace-10' -nb '#353945' -nf '#D3DAE3' -sb '#5294E2' -sf '#D3DAE3'"
set $dmenu-themed "dmenu -i -fn 'monospace-10' -nb '#353945' -nf '#D3DAE3' -sb '#5294E2' -sf '#D3DAE3'"
bindcode $mod+Shift+40 exec $dmenu_run-themed
bindcode $mod+40 exec --no-startup-id i3-dmenu-desktop --dmenu=$dmenu-themed
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# change focus
bindcode $mod+44 focus left
bindcode $mod+45 focus down
bindcode $mod+46 focus up
bindcode $mod+47 focus right

# alternatively, you can use the cursor keys:
bindcode $mod+113 focus left
bindcode $mod+116 focus down
bindcode $mod+111 focus up
bindcode $mod+114 focus right

# move focused window
bindcode $mod+Shift+44 move left
bindcode $mod+Shift+45 move down
bindcode $mod+Shift+46 move up
bindcode $mod+Shift+47 move right

# alternatively, you can use the cursor keys:
bindcode $mod+Shift+113 move left
bindcode $mod+Shift+116 move down
bindcode $mod+Shift+111 move up
bindcode $mod+Shift+114 move right

# split in horizontal orientation
bindcode $mod+43 split h

# split in vertical orientation
bindcode $mod+55 split v

# enter fullscreen mode for the focused container
bindcode $mod+41 fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindcode $mod+39 layout stacking
bindcode $mod+25 layout tabbed
bindcode $mod+26 layout toggle split

# toggle tiling / floating
bindcode $mod+Shift+65 floating toggle

# change focus between tiling / floating windows
bindcode $mod+65 focus mode_toggle

# focus the parent container
bindcode $mod+38 focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindcode $mod+10 workspace $ws1
bindcode $mod+11 workspace $ws2
bindcode $mod+12 workspace $ws3
bindcode $mod+13 workspace $ws4
bindcode $mod+14 workspace $ws5
bindcode $mod+15 workspace $ws6
bindcode $mod+16 workspace $ws7
bindcode $mod+17 workspace $ws8
bindcode $mod+18 workspace $ws9
bindcode $mod+19 workspace $ws10

# move focused container to workspace
bindcode $mod+Shift+10 move container to workspace $ws1
bindcode $mod+Shift+11 move container to workspace $ws2
bindcode $mod+Shift+12 move container to workspace $ws3
bindcode $mod+Shift+13 move container to workspace $ws4
bindcode $mod+Shift+14 move container to workspace $ws5
bindcode $mod+Shift+15 move container to workspace $ws6
bindcode $mod+Shift+16 move container to workspace $ws7
bindcode $mod+Shift+17 move container to workspace $ws8
bindcode $mod+Shift+18 move container to workspace $ws9
bindcode $mod+Shift+19 move container to workspace $ws10

# reload the configuration file
bindcode $mod+Shift+54 reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindcode $mod+Shift+27 restart
# exit i3 (logs you out of your X session)
bindcode $mod+Shift+26 exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindcode 44 resize shrink width 10 px or 10 ppt
        bindcode 45 resize grow height 10 px or 10 ppt
        bindcode 46 resize shrink height 10 px or 10 ppt
        bindcode 47 resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindcode 113 resize shrink width 10 px or 10 ppt
        bindcode 116 resize grow height 10 px or 10 ppt
        bindcode 111 resize shrink height 10 px or 10 ppt
        bindcode 114 resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindcode 36 mode "default"
        bindcode 9 mode "default"
        bindcode $mod+27 mode "default"
}

bindcode $mod+27 mode "resize"

# Color scheme
# class                 border  bground text    indicator child_border
client.focused          #5294E2 #383C4A #D3DAE3 #D3DAE3   #353945
client.focused_inactive #7C817C #353945 #D3DAE3 #4B5162   #7C818C
client.unfocused        #353935 #353945 #7C817C #404552   #383C4A
client.urgent           #C60000 #353945 #FF1010 #900000   #900000
client.placeholder      #000000 #0C0C0C #FFFFFF #000000   #0C0C0C
client.background       #353945

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3blocks # -c $HOME/.config/i3/i3blocks.conf
        position top
# Bar color scheme
        colors {
          background #353945
          statusline #D3DAE3
          separator  #7C817C
      
          focused_workspace  #5294E2 #383C4A #D3DAE3
          active_workspace   #ADADAD #777777 #000000
          inactive_workspace #353945 #353945 #7C818C
          urgent_workspace   #C60000 #353945 #FF1010
          binding_mode       #777777 #900000 #FFFFFF
        }
}
# Switch Keyboard layouts
#exec --no-startup-id "setxkbmap -layout us,ru -option grp:alt_shift_toggle"
# Hide edge borders for single window workspaces
hide_edge_borders smart
# no application borders
for_window [class="Firefox"] border none
# Sreen brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 10 # increase screen brightness
bindsym XF86MonBrightnessDown exec xbacklight -dec 10 # decrease screen brightness
# Press Pause to turn off screen or lock session
bindsym Pause exec "sleep 1 && xset dpms force off"
#bindsym Pause exec "dm-tool lock"
# Volume media keys
bindsym XF86AudioRaiseVolume exec "amixer -q sset Master,0 1+ unmute"; exec "pkill -SIGRTMIN+10 i3blocks"
bindsym XF86AudioLowerVolume exec "amixer -q sset Master,0 1- unmute"; exec "pkill -SIGRTMIN+10 i3blocks"
bindsym XF86AudioMute exec "amixer -q sset Master,0 toggle"; exec "pkill -SIGRTMIN+10 i3blocks"

# Set "desktop" background color (X11 root window)
exec --no-startup-id xsetroot -solid "#353945"
# Notification daemon
exec --no-startup-id dunst 
# authentication agent
exec --no-startup-id /usr/libexec/xfce-polkit
# network-manager applet startup
exec --no-startup-id nm-applet
# set wallpaper
#feh --bg-center /usr/share/backgrounds/images/default-16_10.png
EOF

cat > /etc/i3blocks.conf <<'EOF'
# i3blocks config file
#
# Please see man i3blocks for a complete reference!
# The man page is also hosted at http://vivien.github.io/i3blocks
#
# List of valid properties:
#
# align
# color
# command
# full_text
# instance
# interval
# label
# min_width
# name
# separator
# separator_block_width
# short_text
# signal
# urgent

# Global properties
#
# The top properties below are applied to every block, but can be overridden.
# Each block command defaults to the script name to avoid boilerplate.
command=/usr/libexec/i3blocks/$BLOCK_NAME
separator_block_width=15
markup=none


# CPU usage
#
# The script may be called with -w and -c switches to specify thresholds,
# see the script for details.
[cpu_usage]
label=CPU
interval=10
min_width=CPU: 100.00%
separator=false

[load_average]
interval=10

# Memory usage
#
# The type defaults to "mem" if the instance is not specified.
[memory]
label=MEM
separator=false
interval=30

[memory]
label=SWAP
instance=swap
#separator=false
interval=30

# Disk usage
#
# The directory defaults to $HOME if the instance is not specified.
# The script may be called with a optional argument to set the alert
# (defaults to 10 for 10%).
[disk]
label=~
instance=/home
interval=30
separator=false
[disk]
label=/
instance=/
interval=30

# Network interface monitoring
#
# If the instance is not specified, use the interface used for default route.
# The address can be forced to IPv4 or IPv6 with -4 or -6 switches.
#[iface]
#instance=wlp1s0
#color=#00FF00
#interval=10
#separator=false

#[wifi]
#instance=wlp1s0
#interval=10
#separator=false

[bandwidth]
#instance=wlp1s0
interval=5

# Battery indicator
#
# The battery instance defaults to 0.
[battery]
label=BAT
#label=⚡
#instance=1
interval=30
# ↻ ⌛⚠

# Generic media player support
#
# This displays "ARTIST - SONG" if a music is playing.
# Supported players are: spotify, vlc, audacious, xmms2, mplayer, and others.
#[mediaplayer]
#instance=spotify
#interval=5
#signal=10

# OpenVPN support
#
# Support multiple VPN, with colors.
#[openvpn]
#interval=20

# Temperature
#
# Support multiple chips, though lm-sensors.
# The script may be called with -w and -c switches to specify thresholds,
# see the script for details.
#[temperature]
#command=/usr/lib/i3blocks/temperature -chip dell_smm-virtual-0 -w 60
#label=TEMP
#interval=10

# Date Time
#
[time]
#command=date '+%Y-%m-%d %H:%M:%S'
interval=5

# Key indicators
#
# Add the following bindings to i3 config file:
#
# bindsym --release Caps_Lock exec pkill -SIGRTMIN+11 i3blocks
# bindsym --release Num_Lock  exec pkill -SIGRTMIN+11 i3blocks
#[keyindicator]
#instance=CAPS
#interval=once
#signal=11

#[keyindicator]
#instance=NUM
#interval=once
#signal=11
# Volume indicator
#
# The first parameter sets the step (and units to display)
# The second parameter overrides the mixer selection
# See the script for details.
[volume]
label=VOL
#label=🕪
instance=Master
#instance=PCM
interval=once
signal=10

[username]
command=echo $USER
interval=once
EOF

cat >> /usr/libexec/i3blocks/time << 'EOF'
#!/bin/bash
date '+%Y-%m-%d %H:%M:%S'
if [ ! -z $BLOCK_BUTTON ]
then
	case $BLOCK_BUTTON in
		[1-3])notify-send -u low "Calendar" "$(cal)";;
		4)notify-send -u low "Calendar" "$(cal $(date +%d) $(( $(date +%m) + 1 )) $(date +%Y))";;
		5)notify-send -u low "Calendar" "$(cal $(date +%d) $(( $(date +%m) - 1 )) $(date +%Y))";;
	esac
fi
EOF
chmod +x /usr/libexec/i3blocks/time

cat >> /etc/gtk-2.0/gtkrc << 'EOF'
gtk-icon-theme-name = "Clarity-albus"
gtk-theme-name = "Arc-Dark"
gtk-font-name = "Droid Sans 10"
EOF

cat >> /etc/gtk-3.0/settings.ini << 'EOF'
[Settings]
gtk-theme-name = Arc-Dark
gtk-font-name = Droid Sans 10
gtk-icon-theme-name = Clarity-albus
EOF

cat >  /etc/xdg/dunst/dunstrc << 'EOF'
[global]
    ### Display ###

    # Which monitor should the notifications be displayed on.
    monitor = 0

    # Display notification on focused monitor.  Possible modes are:
    #   mouse: follow mouse pointer
    #   keyboard: follow window with keyboard focus
    #   none: don't follow anything
    #
    # "keyboard" needs a window manager that exports the
    # _NET_ACTIVE_WINDOW property.
    # This should be the case for almost all modern window managers.
    #
    # If this option is set to mouse or keyboard, the monitor option
    # will be ignored.
    follow = mouse

    # The geometry of the window:
    #   [{width}]x{height}[+/-{x}+/-{y}]
    # The geometry of the message window.
    # The height is measured in number of notifications everything else
    # in pixels.  If the width is omitted but the height is given
    # ("-geometry x2"), the message window expands over the whole screen
    # (dmenu-like).  If width is 0, the window expands to the longest
    # message displayed.  A positive x is measured from the left, a
    # negative from the right side of the screen.  Y is measured from
    # the top and down respectively.
    # The width can be negative.  In this case the actual width is the
    # screen width minus the width defined in within the geometry option.
    geometry = "300x5-30+20"

    # Show how many messages are currently hidden (because of geometry).
    indicate_hidden = yes

    # Shrink window if it's smaller than the width.  Will be ignored if
    # width is 0.
    shrink = yes

    # The transparency of the window.  Range: [0; 100].
    # This option will only work if a compositing window manager is
    # present (e.g. xcompmgr, compiz, etc.).
    transparency = 0

    # The height of the entire notification.  If the height is smaller
    # than the font height and padding combined, it will be raised
    # to the font height and padding.
    notification_height = 0

    # Draw a line of "separator_height" pixel height between two
    # notifications.
    # Set to 0 to disable.
    separator_height = 2

    # Padding between text and separator.
    padding = 3

    # Horizontal padding.
    horizontal_padding = 3

    # Defines width in pixels of frame around the notification window.
    # Set to 0 to disable.
    frame_width = 1

    # Defines color of the frame around the notification window.
    frame_color = "#ffffff"

    # Define a color for the separator.
    # possible values are:
    #  * auto: dunst tries to find a color fitting to the background;
    #  * foreground: use the same color as the foreground;
    #  * frame: use the same color as the frame;
    #  * anything else will be interpreted as a X color.
    separator_color = frame

    # Sort messages by urgency.
    sort = yes

    # Don't remove messages, if the user is idle (no mouse or keyboard input)
    # for longer than idle_threshold seconds.
    # Set to 0 to disable.
    # Transient notifications ignore this setting.
    idle_threshold = 120

    ### Text ###

    font = monospace 10

    # The spacing between lines.  If the height is smaller than the
    # font height, it will get raised to the font height.
    line_height = 0

    # Possible values are:
    # full: Allow a small subset of html markup in notifications:
    #        <b>bold</b>
    #        <i>italic</i>
    #        <s>strikethrough</s>
    #        <u>underline</u>
    #
    #        For a complete reference see
    #        <http://developer.gnome.org/pango/stable/PangoMarkupFormat.html>.
    #
    # strip: This setting is provided for compatibility with some broken
    #        clients that send markup even though it's not enabled on the
    #        server. Dunst will try to strip the markup but the parsing is
    #        simplistic so using this option outside of matching rules for
    #        specific applications *IS GREATLY DISCOURAGED*.
    #
    # no:    Disable markup parsing, incoming notifications will be treated as
    #        plain text. Dunst will not advertise that it has the body-markup
    #        capability if this is set as a global setting.
    #
    # It's important to note that markup inside the format option will be parsed
    # regardless of what this is set to.
    markup = full

    # The format of the message.  Possible variables are:
    #   %a  appname
    #   %s  summary
    #   %b  body
    #   %i  iconname (including its path)
    #   %I  iconname (without its path)
    #   %p  progress value if set ([  0%] to [100%]) or nothing
    #   %n  progress value if set without any extra characters
    #   %%  Literal %
    # Markup is allowed
    format = "<b>%s</b>\n%b"

    # Alignment of message text.
    # Possible values are "left", "center" and "right".
    alignment = left

    # Show age of message if message is older than show_age_threshold
    # seconds.
    # Set to -1 to disable.
    show_age_threshold = 60

    # Split notifications into multiple lines if they don't fit into
    # geometry.
    word_wrap = yes

    # When word_wrap is set to no, specify where to ellipsize long lines.
    # Possible values are "start", "middle" and "end".
    ellipsize = middle

    # Ignore newlines '\n' in notifications.
    ignore_newline = no

    # Merge multiple notifications with the same content
    stack_duplicates = true

    # Hide the count of merged notifications with the same content
    hide_duplicate_count = false

    # Display indicators for URLs (U) and actions (A).
    show_indicators = yes

    ### Icons ###

    # Align icons left/right/off
    icon_position = off

    # Scale larger icons down to this size, set to 0 to disable
    max_icon_size = 32

    # Paths to default icons.
    icon_path = /usr/share/icons/Clarity-albus/16x16/status/:/usr/share/icons/Clarity-albus/16x16/devices/

    ### History ###

    # Should a notification popped up from history be sticky or timeout
    # as if it would normally do.
    sticky_history = yes

    # Maximum amount of notifications kept in history
    history_length = 20

    ### Misc/Advanced ###

    # dmenu path.
    dmenu = /usr/bin/dmenu -p dunst:

    # Browser for opening urls in context menu.
    browser = /usr/bin/firefox -new-tab

    # Always run rule-defined scripts, even if the notification is suppressed
    always_run_script = true

    # Define the title of the windows spawned by dunst
    title = Dunst

    # Define the class of the windows spawned by dunst
    class = Dunst

    # Print a notification on startup.
    # This is mainly for error detection, since dbus (re-)starts dunst
    # automatically after a crash.
    startup_notification = false

    ### Legacy

    # Use the Xinerama extension instead of RandR for multi-monitor support.
    # This setting is provided for compatibility with older nVidia drivers that
    # do not support RandR and using it on systems that support RandR is highly
    # discouraged.
    #
    # By enabling this setting dunst will not be able to detect when a monitor
    # is connected or disconnected which might break follow mode if the screen
    # layout changes.
    force_xinerama = false

# Experimental features that may or may not work correctly. Do not expect them
# to have a consistent behaviour across releases.
[experimental]
    # Calculate the dpi to use on a per-monitor basis.
    # If this setting is enabled the Xft.dpi value will be ignored and instead
    # dunst will attempt to calculate an appropriate dpi value for each monitor
    # using the resolution and physical size. This might be useful in setups
    # where there are multiple screens with very different dpi values.
    per_monitor_dpi = false

[shortcuts]

    # Shortcuts are specified as [modifier+][modifier+]...key
    # Available modifiers are "ctrl", "mod1" (the alt-key), "mod2",
    # "mod3" and "mod4" (windows-key).
    # Xev might be helpful to find names for keys.

    # Close notification.
    close = ctrl+space

    # Close all notifications.
    close_all = ctrl+shift+space

    # Redisplay last message(s).
    # On the US keyboard layout "grave" is normally above TAB and left
    # of "1". Make sure this key actually exists on your keyboard layout,
    # e.g. check output of 'xmodmap -pke'
    history = ctrl+grave

    # Context menu.
    context = ctrl+shift+period

[urgency_low]
    # IMPORTANT: colors have to be defined in quotation marks.
    # Otherwise the "#" and following would be interpreted as a comment.
    background = "#353945"
    foreground = "#D3EAD3"
    timeout = 10
    # Icon for notifications with low urgency, uncomment to enable
    #icon = /path/to/icon

[urgency_normal]
    background = "#353945"
    foreground = "#D3DAA3"
    timeout = 10
    # Icon for notifications with normal urgency, uncomment to enable
    #icon = /path/to/icon

[urgency_critical]
    background = "#090000"
    foreground = "#ff0707"
    frame_color = "#ff0000"
    timeout = 0
    # Icon for notifications with critical urgency, uncomment to enable
    #icon = /path/to/icon

# Every section that isn't one of the above is interpreted as a rules to
# override settings for certain messages.
# Messages can be matched by "appname", "summary", "body", "icon", "category",
# "msg_urgency" and you can override the "timeout", "urgency", "foreground",
# "background", "new_icon" and "format".
# Shell-like globbing will get expanded.
#
# SCRIPTING
# You can specify a script that gets run when the rule matches by
# setting the "script" option.
# The script will be called as follows:
#   script appname summary body icon urgency
# where urgency can be "LOW", "NORMAL" or "CRITICAL".
#
# NOTE: if you don't want a notification to be displayed, set the format
# to "".
# NOTE: It might be helpful to run dunst -print in a terminal in order
# to find fitting options for rules.

#[espeak]
#    summary = "*"
#    script = dunst_espeak.sh

#[script-test]
#    summary = "*script*"
#    script = dunst_test.sh

#[ignore]
#    # This notification will not be displayed
#    summary = "foobar"
#    format = ""

#[history-ignore]
#    # This notification will not be saved in history
#    summary = "foobar"
#    history_ignore = yes

#[signed_on]
#    appname = Pidgin
#    summary = "*signed on*"
#    urgency = low
#
#[signed_off]
#    appname = Pidgin
#    summary = *signed off*
#    urgency = low
#
#[says]
#    appname = Pidgin
#    summary = *says*
#    urgency = critical
#
#[twitter]
#    appname = Pidgin
#    summary = *twitter.com*
#    urgency = normal
#
# vim: ft=cfg
EOF

cat > /etc/xdg/termite/config << 'EOF'
[options]
#allow_bold = true
#audible_bell = false
#clickable_url = true
#dynamic_title = true
font = monospace 10
#fullscreen = true
#geometry = 640x480
#icon_name = terminal
#mouse_autohide = false
#scroll_on_output = false
#scroll_on_keystroke = true
# Length of the scrollback buffer, 0 disabled the scrollback buffer
# and setting it to a negative value means "infinite scrollback"
scrollback_lines = 10000
#search_wrap = true
#urgent_on_bell = true
#hyperlinks = false

# $BROWSER is used by default if set, with xdg-open as a fallback
#browser = xdg-open

# "system", "on" or "off"
#cursor_blink = system

# "block", "underline" or "ibeam"
#cursor_shape = block

# Hide links that are no longer valid in url select overlay mode
#filter_unmatched_urls = true

# Emit escape sequences for extra modified keys
#modify_other_keys = false

# set size hints for the window
#size_hints = false

# "off", "left" or "right"
#scrollbar = off

[colors]
# If both of these are unset, cursor falls back to the foreground color,
# and cursor_foreground falls back to the background color.
#cursor = #dcdccc
#cursor_foreground = #ffffff
#
#foreground = #ffffff
#foreground_bold = #ffffff
#background = #000000

# 20% background transparency (requires a compositor)
#background = rgba(63, 63, 63, 0.8)

# If unset, will reverse foreground and background
#highlight = #2fff00
#
## Colors from color0 to color254 can be set

[colors]

# special
foreground      = #ffffff
foreground_bold = #ffffff
cursor          = #ababab
background      = #353945

# black
color0  = #000000
color8  = #010101

# red
color1  = #e92f2f
color9  = #e92f2f

# green
color2  = #0ed839
color10 = #0ed839

# yellow
color3  = #dddd13
color11 = #dddd13

# blue
color4  = #3b48e3
color12 = #3b48e3

# magenta
color5  = #f996e2
color13 = #f996e2

# cyan
color6  = #23edda
color14 = #23edda

# white
color7  = #ababab
color15 = #f9f9f9

[hints]
font = monospace 10
#foreground = #dcdccc
#background = #3f3f3f
#active_foreground = #e68080
#active_background = #3f3f3f
#padding = 2
#border = #3f3f3f
#border_width = 0.5
#roundness = 2.0

# vim: ft=dosini cms=#%s
EOF


cat >> /etc/X11/Xresources << 'EOF'
URxvt.font: xft:monospace:size=10
URxvt.scrollBar: False
URxvt.background: #353945
URxvt.foreground: #ffffff
! special
*.foreground:   #ffffff
*.background:   #353945
*.cursorColor:  #ffffff

! black
*.color0:       #000000
*.color8:       #000000

! red
*.color1:       #e92f2f
*.color9:       #e92f2f

! green
*.color2:       #0ed839
*.color10:      #0ed839

! yellow
*.color3:       #dddd13
*.color11:      #dddd13

! blue
*.color4:       #5249e2
*.color12:      #5249e2

! magenta
*.color5:       #f996e2
*.color13:      #f996e2

! cyan
*.color6:       #23edda
*.color14:      #23edda

! white
*.color7:       #ababab
*.color15:      #f9f9f9
EOF

cat >> /etc/xdg/Trolltech.conf << 'EOF'
[Qt]
style=GTK+
EOF

cat >> /etc/profile.d/qt5stylegtk.sh << 'EOF'
export QT_QPA_PLATFORMTHEME=gtk2
EOF

%end

