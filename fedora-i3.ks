# fedora-livecd-xfce.ks
#
# Description:
# - Fedora Live Spin with the light-weight XFCE Desktop Environment
#
# Maintainer(s):
# - Rahul Sundaram    <sundaram@fedoraproject.org>
# - Christoph Wickert <cwickert@fedoraproject.org>
# - Kevin Fenzi       <kevin@tummy.com>
# - Adam Miller       <maxamillion@fedoraproject.org>


%packages
i3
i3blocks
dunst
# Termite requires a patched version of vte:
termite
@networkmanager-submodules
gnome-keyring-pam
gparted
setroubleshoot
lm_sensors
sysstat
xfce-polkit
lightdm-gtk
light-locker
wget
git
qt5-qtstyleplugins
adwaita-gtk2-theme
adwaita-icon-theme
arc-theme
google-noto-sans-fonts
google-noto-serif-fonts
mononoki-ttf-fonts
clarity-albus-icon-theme
initial-setup-gui
network-manager-applet
vim-enhanced
qutebrowser
rpmfusion-free-release
rpmfusion-nonfree-release
# save some space
-autofs
-acpid
-gimp-help
-desktop-backgrounds-basic
-aspell-*                   # dictionaries are big
-foomatic-db-ppds
-dejavu*fonts
-openbox

%end

%post
# Enable copr repos:
cat > /etc/yum.repos.d/_copr_wyvie-i3blocks.repo << 'EOF'
[wyvie-i3blocks]
name=Copr repo for i3blocks owned by wyvie
baseurl=https://copr-be.cloud.fedoraproject.org/results/wyvie/i3blocks/fedora-$releasever-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://copr-be.cloud.fedoraproject.org/results/wyvie/i3blocks/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
EOF
cat > /etc/yum.repos.d/_copr_skidnik-termite.repo << 'EOF'
[skidnik-termite]
name=Copr repo for termite owned by skidnik
baseurl=https://copr-be.cloud.fedoraproject.org/results/skidnik/termite/fedora-$releasever-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://copr-be.cloud.fedoraproject.org/results/skidnik/termite/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
EOF
cat > /etc/yum.repos.d/_copr_skidnik-clarity-icon-theme.repo << 'EOF'
[skidnik-clarity-icon-theme]
name=Copr repo for clarity-icon-theme owned by skidnik
baseurl=https://copr-be.cloud.fedoraproject.org/results/skidnik/clarity-icon-theme/fedora-$releasever-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://copr-be.cloud.fedoraproject.org/results/skidnik/clarity-icon-theme/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
EOF

# Override monospace font aliases to mononoki
ln -s /usr/share/fontconfig/conf.avail/56-mononoki.conf /etc/fonts/conf.d/56-mononoki.conf

cat >> /etc/profile.d/editor.sh << 'EOF'
#!/bin/bash
# Set default text editor
export EDITOR=vim
EOF

cat >> /etc/profile.d/xterminal.sh << 'EOF'
#!/bin/bash
# Set default terminal emulator
export TERMINAL=termite
EOF

cat >> /usr/share/fontconfig/conf.avail/56-noto-sans.conf << 'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fontconfig SYSTEM "../fonts.dtd">
<!-- /etc/fonts/conf.d/57-dejavu-sans.conf

     Define aliasing and other fontconfig settings for
     Noto Sans.

     © 2006-2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
-->
<fontconfig>
  <!-- Font substitution rules -->
  <alias binding="same">
    <family>Arev Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Bepa</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Bitstream Prima Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Bitstream Vera Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>DejaVu LGC Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Hunky Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Olwen Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>SUSE Sans</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Verajja</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <!-- In case VerajjaPDA stops declaring itself as Verajja -->
  <alias binding="same">
    <family>VerajjaPDA</family>
    <accept>
      <family>Noto Sans</family>
    </accept>
  </alias>
  <!-- Generic name assignment -->
  <alias>
    <family>Noto Sans</family>
    <default>
      <family>sans-serif</family>
    </default>
  </alias>
  <!-- Generic name aliasing -->
  <alias>
    <family>sans-serif</family>
    <prefer>
      <family>Noto Sans</family>
    </prefer>
  </alias>
</fontconfig>
EOF

cat >> /usr/share/fontconfig/conf.avail/56-noto-serif.conf << 'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE fontconfig SYSTEM "../fonts.dtd">
<!-- /etc/fonts/conf.d/57-dejavu-serif.conf

     Define aliasing and other fontconfig settings for
     Noto Serif.

     © 2006-2008 Nicolas Mailhot <nicolas.mailhot at laposte.net>
-->
<fontconfig>
  <!-- Font substitution rules -->
  <alias binding="same">
    <family>Bitstream Prima Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Bitstream Vera Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>DejaVu LGC Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Hunky Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>Olwen Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <alias binding="same">
    <family>SUSE Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <!-- In case Verajja Serif stops declaring itself as Noto Serif -->
  <alias binding="same">
    <family>Verajja Serif</family>
    <accept>
      <family>Noto Serif</family>
    </accept>
  </alias>
  <!-- Generic name assignment -->
  <alias>
    <family>Noto Serif</family>
    <default>
      <family>serif</family>
    </default>
  </alias>
  <!-- Generic name aliasing -->
  <alias>
    <family>serif</family>
    <prefer>
      <family>Noto Serif</family>
    </prefer>
  </alias>
</fontconfig>
EOF

# The above font configs override font aliases and common fonts to google-noto instead of ugly DejaVu, enable them:

ln -s /usr/share/fontconfig/conf.avail/56-noto-sans.conf /etc/fonts/conf.d/56-noto-sans.conf
ln -s /usr/share/fontconfig/conf.avail/56-noto-serif.conf /etc/fonts/conf.d/56-noto-serif.conf

%end

