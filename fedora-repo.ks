# Include the appropriate repo definitions

# Exactly one of the following should be uncommented

# For the master branch the following should be uncommented
# %include fedora-repo-rawhide.ks

# For non-master branches the following should be uncommented
%include fedora-repo-not-rawhide.ks
# Add rpmfusion repos
%include fedora-repo-rpmfusion.ks
# Add wyvie copr repos
%include fedora-repo-wyvie.ks
# Add skidnik repos
%include fedora-repo-skidnik.ks
